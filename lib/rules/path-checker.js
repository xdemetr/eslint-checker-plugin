'use strict';
const path = require('path');
const { isPathRelative } = require('../helpers');

module.exports = {
  meta: {
    type: null, // `problem`, `suggestion`, or `layout`
    docs: {
      description: 'FSD relative paths checker',
      recommended: false,
      url: null,
    },
    fixable: 'code',
    schema: [
      {
        type: 'object',
        properties: {
          alias: {
            type: 'string',
          },
        },
      },
    ],
  },

  create(context) {
    const alias = context.options[0]?.alias || '';

    return {
      ImportDeclaration(node) {
        try {
          /**
           Example: app/entities/Article
           */
          const value = node.source.value;
          const importTo = alias ? value.replace(`${alias}/`, '') : value;

          /**
           * Example users/dd/dev/project/src/entities/Article/Article.tsx
           */
          const fromFileName = context.getFilename();

          if (shouldBeRelative(fromFileName, importTo)) {
            context.report({
              node,
              message: 'В рамках одного слайса импорты должно быть относительными',
              fix: (fixer) => {

                //entities/Article/Article.tsx
                const normalizedPath = getNormalizedCurrentPath(fromFileName)
                  .split('/')
                  // Убираем Article.tsx
                  .slice(0, -1)
                  // Склеиваем обратно
                  .join('/');

                let relativePath = path.relative(normalizedPath, `/${importTo}`);

                if (!relativePath.startsWith('.')) {
                  relativePath = `./${relativePath}`;
                }

                return fixer.replaceText(node.source, `'${relativePath}'`);
              },
            });
          }
        } catch (e) {
          console.log(e);
        }
      },
    };
  },
};

const layers = {
  'entities': 'entities',
  'features': 'features',
  'pages': 'pages',
  'shared': 'shared',
  'widgets': 'widgets',
};

function getNormalizedCurrentPath(from = '') {
  const normalizedPath = path.toNamespacedPath(from);
  const projectFrom = normalizedPath.split('src')[1];

  return projectFrom.split(/\\|\//).join('/');
}

function shouldBeRelative(from, to = '') {
  if (isPathRelative(to)) {
    return false;
  }
  const toArray = to.split('/');
  const toLayer = toArray[0]; // entities
  const toSlice = toArray[1]; // Article

  if (!toLayer || !toSlice || !layers[toLayer]) {
    return false;
  }

  // const normalizedPath = path.toNamespacedPath(from);
  // const projectFrom = normalizedPath.split('src')[1];
  // const fromArray = projectFrom.split(/\\|\//);


  const fromArray = getNormalizedCurrentPath(from).split('/');
  const fromLayer = fromArray[1];
  const fromSlice = fromArray[2];

  if (!fromLayer || !fromSlice || !layers[fromLayer]) {
    return false;
  }

  const isRelativePath = fromSlice === toSlice && toLayer === fromLayer;

  return isRelativePath;
}
