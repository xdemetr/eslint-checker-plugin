/**
 * @fileoverview description
 * @author dd
 */
'use strict';

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

const rule = require('../../../lib/rules/public-api-imports'),
  RuleTester = require('eslint').RuleTester;


//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

const ruleTester = new RuleTester({
  parserOptions: { ecmaVersion: 6, sourceType: 'module' },
});

const aliasOptions = [{
  alias: '@',
}];

ruleTester.run('public-api-imports', rule, {
  valid: [
    {
      code: 'import { addNewCommentActions, addNewCommentReducer } from \'../../model/slices/addNewCommentSlice\';',
      errors: [],
    },
    {
      code: 'import { addNewCommentActions, addNewCommentReducer } from \'@/entities/Article\';',
      errors: [],
      options: aliasOptions,
    },
    {
      code: 'import { useEffect } from \'react\';',
      errors: [],
      options: [{
        alias: '@',
        testFilesPatterns: ['**/*.test.ts', '**/*.test.ts', '**/StoreDecorator.tsx']
      }],
    },
    {
      filename: '/Users/dd/dev/project/src/entities/StoreDecorator.tsx',
      code: "import { addCommentFormActions, addCommentFormReducer } from '@/entities/Article/testing'",
      errors: [],
      options: [{
        alias: '@',
        testFilesPatterns: ['**/*.test.ts', '**/*.test.tsx', '**/StoreDecorator.tsx']
      }],
    }
  ],

  invalid: [
    {
      filename: '/Users/dd/dev/project/src/features/AddNewComment/AddNewComment.tsx',
      code: 'import { addNewCommentActions, addNewCommentReducer } from \'@/entities/Article/model/slide/addCommentSlice\';',
      errors: [{ message: 'Абсолютный импорт разрешен только из publicApi (index.ts)' }],
      options: [{
        alias: '@',
        testFilesPatterns: ['**/*.test.ts', '**/*.test.tsx', '**/StoreDecorator.tsx']
      }],
    },
    {
      filename: '/Users/dd/dev/project/src/features/AddNewComment/AddNewComment.tsx',
      code: "import { addCommentFormActions, addCommentFormReducer } from '@/entities/Article/testing'",
      errors: [{message: 'Тестовые данные необходимо импортировать из publicApi/testing'}],
      options: [{
        alias: '@',
        testFilesPatterns: ['**/*.test.ts', '**/*.test.tsx', '**/StoreDecorator.tsx']
      }],
    },
    {
      filename: '/Users/dd/dev/project/src/entities/StoreDecorator.tsx',
      code: "import { addCommentFormActions, addCommentFormReducer } from '@/entities/Article/testing/file.tsx'",
      errors: [{message: 'Абсолютный импорт разрешен только из publicApi (index.ts)'}],
      options: [{
        alias: '@',
        testFilesPatterns: ['**/*.test.ts', '**/*.test.tsx', '**/StoreDecorator.tsx']
      }],
    },
  ],
});
