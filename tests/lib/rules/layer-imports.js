const rule = require('../../../lib/rules/layer-imports'),
  RuleTester = require('eslint').RuleTester;

const aliasOptions = [
  {
    alias: '@',
  },
];
const ruleTester = new RuleTester({
  parserOptions: { ecmaVersion: 6, sourceType: 'module' },
});
ruleTester.run('layer-imports', rule, {
  valid: [
    {
      filename: '/Users/dd/dev/project/src/entities/Article/ui/ArticleDetails/ArticleDetails.tsx',
      code: 'import { addCommentFormActions, addCommentFormReducer } from \'@/shared/Button\'',
      errors: [],
      options: aliasOptions,
    },
    // Windows
    {
      filename: 'C:\\Users\\dd\\Desktop\\project\\src\\features\\Article',
      code: 'import { addCommentFormActions, addCommentFormReducer } from \'@/shared/Button\'',
      errors: [],
      options: aliasOptions,
    },
    {
      filename: '/Users/dd/dev/project/src/entities/Article/ui/ArticleDetails/ArticleDetails.tsx',
      code: 'import { addCommentFormActions, addCommentFormReducer } from \'@/entities/Article\'',
      errors: [],
      options: aliasOptions,
    },
    {
      filename: '/Users/dd/dev/project/src/entities/Article/ui/ArticleDetails/ArticleDetails.tsx',
      code: 'import { Sidebar } from \'@/shared/Sidebar/\'',
      errors: [],
      options: aliasOptions,
    },
    {
      filename: '/Users/dd/dev/project/src/entities/Article/ui/ArticleDetails/ArticleDetails.tsx',
      code: 'import { useLocation } from \'react-router-dom\'',
      errors: [],
      options: aliasOptions,
    },
    {
      filename: '/Users/dd/dev/project/src/entities/Article/ui/ArticleDetails/ArticleDetails.tsx',
      code: 'import { addCommentFormActions, addCommentFormReducer } from \'redux\'',
      errors: [],
      options: aliasOptions,
    },
    {
      filename: '/Users/dd/dev/project/src/index.tsx',
      code: 'import { StoreProvider } from \'@/app/providers/StoreProvider\';',
      errors: [],
      options: aliasOptions,
    },
    {
      filename: '/Users/dd/dev/project/src/entities/Article/ui/ArticleDetails/ArticleDetails.tsx',
      code: 'import { StateSchema } from \'@/app/providers/StoreProvider\'',
      errors: [],
      options: [
        {
          alias: '@',
          ignoreImportPatterns: ['**/StoreProvider'],
        },
      ],
    },
    {
      filename: '/Users/dd/dev/project/src/entities/Article/ui/ArticleDetails/ArticleDetails.tsx',
      code: 'import { StateSchema } from \'@/app/providers/StoreProvider\'',
      errors: [],
      options: [
        {
          alias: '@',
          ignoreImportPatterns: ['**/StoreProvider'],
        },
      ],
    },
  ],

  invalid: [
    {
      filename: '\\Users\\dd\\dev\\project\\src\\entities\\providers\\index.tsx',
      code: 'import { addCommentFormActions, addCommentFormReducer } from \'@/features/Article\'',
      errors: [{ message: 'Слой может импортировать в себя только нижележащие слои (shared, entities, features, widgets, pages, app)' }],
      options: aliasOptions,
    },
    {
      filename: '\\Users\\dd\\dev\\project\\src\\entities\\features\\index.tsx',
      code: 'import { addCommentFormActions, addCommentFormReducer } from \'@/widgets/Navbar\'',
      errors: [{ message: 'Слой может импортировать в себя только нижележащие слои (shared, entities, features, widgets, pages, app)' }],
      options: aliasOptions,
    },
    {
      filename: '\\Users\\dd\\dev\\project\\src\\entities\\providers\\index.tsx',
      code: 'import { addCommentFormActions, addCommentFormReducer } from \'@/widgets/Navbar\'',
      errors: [{ message: 'Слой может импортировать в себя только нижележащие слои (shared, entities, features, widgets, pages, app)' }],
      options: aliasOptions,
    },
    {
      filename: '/Users/dd/dev/project/src/widgets/ThemeSwitcher/ui/ThemeSwitcher.stories.tsx',
      code: 'import { Theme } from \'@/app/providers/ThemeProvider\';',
      errors: [{ message: 'Слой может импортировать в себя только нижележащие слои (shared, entities, features, widgets, pages, app)' }],
      options: aliasOptions,
    },
  ],
});
