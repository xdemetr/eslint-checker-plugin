/**
 * @fileoverview FSD relative paths checker
 * @author dd
 */
'use strict';

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

const rule = require('../../../lib/rules/path-checker'),
  RuleTester = require('eslint').RuleTester;


//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

const ruleTester = new RuleTester({
  parserOptions: { ecmaVersion: 6, sourceType: 'module' },
});
ruleTester.run('path-checker', rule, {
  valid: [
    {
      filename: '/Users/dd/dev/project/src/features/AddNewComment/index.ts',
      code: 'import { addNewCommentActions, addNewCommentReducer } from \'../../model/slices/addNewCommentSlice\';',
      errors: [],
    },
  ],

  invalid: [
    {
      filename: '/Users/dd/dev/project/src/features/AddNewComment/index.tsx',
      code: 'import { addNewCommentActions, addNewCommentReducer } from \'features/AddNewComment/model/slices/addNewCommentSlice\';',
      errors: [{ message: 'В рамках одного слайса импорты должно быть относительными' }],
    },
    {
      filename: '/Users/dd/dev/project/src/features/AddNewComment',
      code: 'import { addNewCommentActions, addNewCommentReducer } from \'@/features/AddNewComment/model/slices/addNewCommentSlice\';',
      errors: [{ message: 'В рамках одного слайса импорты должно быть относительными' }],
      options: [{
         alias: '@'
      }],
    },
  ],
});
