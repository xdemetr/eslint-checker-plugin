# eslint-plugin-dd-plugin

plugin checker

## Installation

You'll first need to install [ESLint](https://eslint.org/):

```sh
npm i eslint --save-dev
```

Next, install `eslint-plugin-dd-plugin`:

```sh
npm install eslint-plugin-dd-plugin --save-dev
```

## Usage

Add `dd-plugin` to the plugins section of your `.eslintrc` configuration file. You can omit the `eslint-plugin-` prefix:

```json
{
    "plugins": [
        "dd-plugin"
    ]
}
```


Then configure the rules you want to use under the rules section.

```json
{
    "rules": {
        "dd-plugin/path-checker": ["error", { alias: "@" }],
        "dd-plugin/public-api-imports": ["error", { alias: "@" }],
    }
}
```
